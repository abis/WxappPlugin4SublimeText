const configGlobal = require('../../config/config_global.js');
var util = require('../../utils/function.js');
var app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo: null,

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log('onLoad');
        var that = this;

        // 赋值
        that.setData({
            userInfo: app.globalData.userInfo
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {
        console.log('onReady');
        var that = this;


        // 请求服务器
        wx.showLoading({
            title: '加载中...',
            mask: true
        });

        // 设置参数
        var data_param = {};
        data_param.openId = app.globalData.openId;
        var param = util.getRequestData(data_param);

        wx.request({
            //必需
            url: configGlobal.request_url + '/_WxaappApiServer/',
            method: 'GET',
            data: {
                appid: param.appid,
                timestamp: param.timestamp,
                sign: param.sign,
                data: param.data,
            },
            header: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            success: function(res) {
                wx.hideLoading();
                console.log('服务器返回 success：', res);

                if (result.data.code == '0') {
                    var data = res.data.data;

                    that.setData({});
                } else {
                    console.log('获取失败！');
                    wx.showToast({
                        title: '获取失败！',
                        mask: true,
                        duration: 2000
                    });
                }
            },
            fail: function(res) { console.log('request fail res:', res); },
            complete: function(res) { wx.hideLoading(); }
        });

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        console.log('onShow');
        var that = this;

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {
        console.log('onHide');
        var that = this;

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {
        console.log('onUnload');
        var that = this;

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {
        console.log('onPullDownRefresh');
        var that = this;
        // 这儿写你的下拉要执行的代码

        // 停止下拉
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        console.log('onReachBottom');
        var that = this;

    },

    /**
     * 页面转发分享--监听用户转发分享动作
     */
    onShareAppMessage: function(res) {
        console.log('res:', res);
        var that = this;

        var shareTitle = '这是转发分享标题';
        var sharePath = '/pages/';
        var shareDesc = '';
        var imageUrl = '';

        return {
            title: shareTitle,
            path: sharePath,
            // imageUrl: imageUrl,
            desc: shareDesc,
            success: function(res) {
                console.log('success res:', res);
                if (res.errMsg == 'shareAppMessage:ok') {
                    var shareTickets = res.shareTickets;

                } else if (res.errMsg == 'shareAppMessage:fail cancel') {

                } else if (res.errMsg == 'shareAppMessage:fail cancel') {

                }
            },
            fail: function(res) {},
            complete: function(res) {}
        };

    },

})