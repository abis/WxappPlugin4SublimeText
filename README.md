# WxappPlugin4SublimeText
SublimeText的微信小程序的插件，能自动提示和代码补全。
#### 注意：wxappDEMO并非实际的项目，仅仅是展示目录结构，多次迭代，可能里面存在错误，不用管它！


## 一、插件安装
### 1.首先安装Sublime Text，下载地址为：
```html
http://www.sublimetext.com/
```

### 2.将wxappPlugin目录拷贝到sublime text的安装目录下的
```html
Data\Packages\
```
目录下即可使用

## 二、如何使用
### 1.生成js整个文件代码
在js文件中键入
```html
appjs---得到一个app.js格式文件
pagejs---得到一个page.js格式文件
```

### 2.js中的代码自动提示
比如数据请求，键入：wxrequest（或前几个字母，忽略大小写）回车后会自动出来:
```javascript
// 请求服务器
wx.showLoading({
    title: '加载中...',
    mask: true
});

wx.request({
    url: configGlobal.request_url + '/_WxaappApiServer/',
    method: 'GET',
    header: {
        'Content-Type': 'application/json; charset=utf-8'
    },
    data: {
        openId: app.globalData.openId,

    },
    success: function(res) {
        wx.hideLoading();
        console.log('服务器返回 success：', res);

        if (res.data.result == true) {
            var data = res.data.data;

            that.setData({
            });
        }else{
            console.log('获取失败！');
            wx.showToast({
                title: '获取失败！',
                mask: true,
                duration: 2000
            });
        }
    },
    fail: function(res) { console.log('request fail res:', res); },
    complete: function(res) { wx.hideLoading(); }
});

```

比如微信支付键入：wxrequestPayment（或前几个字母，忽略大小写）回车后会自动出来:
```javascript
// 商品名称设置
var body = '这是测试商品';
var attach = '真假酒水';
var total_fee = '1';


// 请求服务器
wx.showLoading({
    title: '加载中...',
    mask: true
});

// 设置参数
var data_param = {};
data_param.openId = app.globalData.openId;
data_param.body = body;
data_param.attach = attach;
data_param.total_fee = total_fee;
var param = util.getRequestData(data_param);

wx.request({
    //必需
    url: configGlobal.request_url + '/_WxaappApiServer/payGenerateOrder',
    method: 'GET',
    data: {
        openId: app.globalData.openId,
        body: body,
        attach: attach,
        total_fee: total_fee,
    },
    header: {
        'Content-Type': 'application/json; charset=utf-8'
    },
    success: function(res) {
        wx.hideLoading();
        console.log('服务器返回 success：', res);

        if (res.data.code == '0') {
            var data = res.data.data;
            console.log('data：', data);

            // 调用支付
            wx.requestPayment({
                'timeStamp': data.timeStamp,
                'nonceStr': data.nonceStr,
                'package': data.package,
                'signType': 'MD5',
                'paySign': data.paySign,
                success: function(res) {
                    console.log('success res:', res);
                    if (res.errMsg == 'requestPayment:ok') {
                        // 支付成功

                        that.setData({

                        });
                        wx.showToast({
                            title: "支付成功",
                            icon: "success", //仅支持success或者loading
                            duration: 2000,
                        });
                    } else if (res.errMsg == 'requestPayment:fail cancel') {
                        // 取消支付
                        wx.showToast({
                            title: "支付已被取消",
                            icon: "success", //仅支持success或者loading
                            duration: 2000,
                        });
                    } else if (res.errMsg == 'requestPayment:fail') {
                        // 支付失败
                        wx.showToast({
                            title: "支付失败",
                            icon: "success", //仅支持success或者loading
                            duration: 2000,
                        });
                    }
                },
                fail: function(res) {
                    console.log('fail res:', res);
                    // 支付失败
                    wx.showToast({
                        title: "支付失败",
                        icon: "success", //仅支持success或者loading
                        duration: 2000,
                    });
                },
                complete: function(res) {
                    // console.log('complete res:', res);

                }
            });

        } else {
            wx.showToast({
                title: "从服务器获取订单参数失败！",
                icon: "success", //仅支持success或者loading
                duration: 2000,
            });
        }
    },
    fail: function(res) { console.log('request fail res:', res); },
    complete: function(res) { wx.hideLoading(); }
});

```
比如：thatSetData
```javascript
that.setData({

});

```
比如：consolelog
```javascript
console.log('res：', res);

```
比如：varthat
```javascript
var that = this;

```

比如：varsession（或前几个字母，忽略大小写）回车后会自动出来:
```javascript
// 在需要得到用户信息的地方做下判断
var session = app.getSession();
if (!session) {
    app.getAuthorize();
} else {
    that.setData({
        userInfo: app.globalData.userInfo
    });

}

```
等等等等。。。。。。

### 3.wxml文件中的提示
比如你在wxml中键入：<view （或前几个字母，忽略大小写）回车后会自动出来:
```html
<view class="" >

</view>
```
比如你在wxml中键入：<blockif （或前几个字母，忽略大小写）回车后会自动出来:
```html
<block wx:if="{{}}">

</block>
```
如果你选择后缀带_tip的，比如：<button_tip （或前几个字母，忽略大小写）回车后会自动出来:
```html
<!-- 将下面的代码放到JS文件中
 /**
  * 绑定的button事件
  * @param {Object} event
  */
 bindButtonTap: function(event) {
     console.log('bindButtonTap: ', event);
     var that = this;

 },
-->
<button type="primary" size="default" bindtap="bindButtonTap" formType="submit">提交</button>

```

## 三、目录结构
查看wxappDEMO中的例子，建议按其设定目录结构

# 备注
本插件如发现错误，或在使用的过程中若有困难，欢迎与我联系，我会及时修正。

```html
###联系作者
制作：宿迁老葛
QQ：9411526
微信(同QQ号)：9411526
email: 9411526@qq.com

###欢迎加入微信群：
微信技术开发交流群：加群方法，请先加宿迁老葛为好友，由宿迁老葛邀请您入微信群。加好友时请注明入小程序群字样。

###欢迎加QQ群：
微信技术开发交流：162384579(收费)
微信小程序开发交流：236089949(免费)
```
注：加好友时请注明小程序字样。


仅在码云上发布，地址为：

```html
https://gitee.com/abis/WxappPlugin4SublimeText
```